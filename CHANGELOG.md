## Abandoned Flat Containers Changelog

#### Version 2.3

* Fixed incorrect usage of DynamicMusic's system for ignoring cells

<!-- [Download Link](https://gitlab.com/modding-openmw/abandoned-flat-containers/-/packages/#TODO) | [Nexus](https://www.nexusmods.com/morrowind/mods/53142) -->

#### Version 2.2

* Fixed a problem that could prevent being teleported back to the storage area after the boss fight

[Download Link](https://gitlab.com/modding-openmw/abandoned-flat-containers/-/packages/25351120) | [Nexus](https://www.nexusmods.com/morrowind/mods/53142)

#### Version 2.1

* Fixed key sorting
* Moved some banners so they don't clip into the wall

[Download Link](https://gitlab.com/modding-openmw/abandoned-flat-containers/-/packages/21213569) | [Nexus](https://www.nexusmods.com/morrowind/mods/53142)

#### Version 2.0

* Total rewrite of the mod: all new, expanded Enchanting Storage interior with assets from [OAAB_Data](https://www.nexusmods.com/morrowind/mods/49042) and [Tamriel_Data](https://www.nexusmods.com/morrowind/mods/44537) and more typed containers -- including a "take all" activator which takes everything all at once
* Several easter eggs including a superboss fight (see the README for spoiler-ridden details!)

[Download Link](https://gitlab.com/modding-openmw/abandoned-flat-containers/-/packages/21213376) | [Nexus](https://www.nexusmods.com/morrowind/mods/53142)

#### Version 1.1

* Fixed a bug that caused the cube activators to not work after save/load.

[Download Link](https://gitlab.com/modding-openmw/abandoned-flat-containers/-/packages/15736802) | [Nexus](https://www.nexusmods.com/morrowind/mods/53142)

#### Version 1.0

Initial release of the mod.

[Download Link](https://gitlab.com/modding-openmw/abandoned-flat-containers/-/packages/15714125) | [Nexus](https://www.nexusmods.com/morrowind/mods/53142)
