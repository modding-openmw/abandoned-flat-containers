#!/bin/sh
set -eu

#
# This script packages the project into a zip file.
#

file_name=abandoned-flat-containers.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags || git rev-parse --short HEAD)
EOF

zip --must-match --recurse-paths ${file_name} \
    CHANGELOG.md \
    LICENSE \
    README.md \
    Abandoned_Flat_Containers.omwaddon \
    Abandoned_Flat_Containers.omwscripts \
    scripts \
    version.txt \
    --exclude \*.mwscript

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
