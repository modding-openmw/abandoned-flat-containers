# Abandoned Flat Containers

An add-on for the [Abandoned Flat](https://modding-openmw.gitlab.io/abandoned-flat/) player home mod that expandes a special storage cell to have containers with massive storage capacity. This interior cell has been totally expanded and revamped with assets from [OAAB_Data](https://www.nexusmods.com/morrowind/mods/49042) and [Tamriel_Data](https://www.nexusmods.com/morrowind/mods/44537) and a new boss fight. An additional feature adds Lua-powered inventory helper cubes that deposit items of certain types into designated containers.

**Requires OpenMW 0.49 or newer**!

#### Credits

* **Author**: johnnyhostile

**Special Thanks**:

* [Ghost Who Walks](http://www.ghostwhowalks.net/) for making the original Abandoned Flat
* **Benjamin Winger** for making [Delta Plugin](https://gitlab.com/bmwinger/delta-plugin/)
* **EvilEye** for making [CS.js](https://assumeru.gitlab.io/cs.js/) (used to make the quest)
* **Greatness7** for making [tes3conv](https://github.com/Greatness7/tes3conv)
* **ptmikheev** for OpenMW-Lua API help
* **zackhasacat** for OpenMW-Lua API help
* **The Morrowind Modding Community and the OAAB_Data Team** for making [OAAB_Data](https://www.nexusmods.com/morrowind/mods/49042)
* **The Tamriel Rebuilt Team** for making [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/)
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/abandoned-flat-containers/)

[Nexus Mods](https://www.nexusmods.com/morrowind/mods/53142)

[Source on GitLab](https://gitlab.com/modding-openmw/abandoned-flat-containers)

#### Installation

1. Download the zip from a link above
1. Extract the contents to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\PlayerHomes\abandoned-flat-containers

        # Linux
        /home/username/games/OpenMWMods/PlayerHomes/abandoned-flat-containers

        # macOS
        /Users/username/games/OpenMWMods/PlayerHomes/abandoned-flat-containers
1. Add the appropriate data path to your `openmw.cfg` file, for example:

        data="C:\games\OpenMWMods\PlayerHomes\abandoned-flat-containers"
1. Enable the mod plugins via OpenMW-Launcher ([video](https://www.youtube.com/watch?v=xzq_ksVuRgc)), or add this to `openmw.cfg` ([official OpenMW documentation](https://openmw.readthedocs.io/en/latest/reference/modding/mod-install.html#install)):

        content=Abandoned_Flat_Containers.omwaddon
        content=Abandoned_Flat_Containers.omwscripts

#### Easter Egg Explanations

**Spoiler Alert!!** Highlight to read, warning there are links in the spoiler text so highlight carefully.

* <span class="spoiler">Ferguson and the quest name "Taking Liberties": Both are from an episode of the show "Frasier" called "Taking Liberties" ([1](https://www.frasieronline.co.uk/episodeguide/season8/ep4.htm)). "The List" is from the "Frasier" episode "Door Jam" ([2](https://www.frasieronline.co.uk/episodeguide/season10/ep11.htm))</span>
* <span class="spoiler">Ozzie, Flea, and Slash: Three bosses from Chrono Trigger. Yes they are named after rock musicians, yes that's from Chrono Trigger too. The rewards obtained after defeating them are inspired by items you can steal from them in Chrono Trigger. ([1](https://www.chronowiki.org/wiki/Ozzie), [2](https://www.chronowiki.org/wiki/Flea) , [3](https://www.chronowiki.org/wiki/Slash))</span>
* <span class="spoiler">To hear a special boss battle song during the fight with Ozzie, Flea, and Slash: 1) [Buy The Chrono Trigger Symphony](https://thechronosymphony.com/), 2) Download the .flac version and extract it to some folder, 3) Add that folder as a data path, 4) When the boss battle starts a song from Chrono Trigger will play.</span>
* <span class="spoiler">NOTE! This boss battle is recommended for very high-level characters, 40 or above. There also may be a way to skip the battle and gain access to Enchanting Storage anyways...</span>

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/abandoned-flat-containers/-/issues/new) for bug reports or feature requests
* Email: `abandoned-flat-containers at modding-openmw dot com`
* Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/53142?tab=posts)
