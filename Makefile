build:
	./build.sh

pkg: build
	./pkg.sh

clean:
	rm -f *.zip *.sha*sum.txt version.txt *.omwaddon

web-clean:
	cd web && rm -rf build site/*.md sha256sums*

web: web-clean
	cd web && ./build.sh

web-debug: web-clean
	cd web && ./build.sh --debug

web-verbose: web-clean
	cd web && ./build.sh --verbose

clean-all: clean web-clean

local-web:
	cd web && python3 -m http.server -d build

save-to-json:
	tes3conv -o Abandoned_Flat_Containers.omwaddon Abandoned_Flat_Containers.json

save-to-yaml:
	delta_plugin convert -o . Abandoned_Flat_Containers.omwaddon
